var config = require('./config.js'),
    serialport = require("serialport"),
    SerialPort = serialport.SerialPort;

var gui = require('nw.gui');
var win = gui.Window.get();

serialport.list(function (err, ports) {
    ports.forEach(function(port) {
        console.log(port.comName);
    });
});

function Robot() {
    this.wheels = [];
    this.maxSpeed = 2;
    this.maxRotation = 4;
    this.kickStrength = 5000;

    this.speed = 0;
    this.angle = 0;
    this.rotation = 0;

    this.serialPort = null;
    this.sendReady = false;
}

Robot.prototype.init = function () {
    this.findSerialPort(function (err, portName) {
        if (!err && portName !== null) {
            this.serialPort = new SerialPort(portName, {
                baudrate: 115200,
                parser: serialport.parsers.readline('\n')
            });

            this.serialPort.open(function (err) {
                console.log('open');

                if (!err) {
                    this.sendReady = true;

                    this.serialPort.on('data', function(data) {
                        console.log('data received: ' + data);
                    });

                    this.serialPort.close(function (err) {
                        console.log('close');
                        if (err) {
                            this.sendReady = false;
                            console.log(err);
                        }
                    }.bind(this));

                } else {
                    console.log(err);
                }

            }.bind(this));
        } else {
            console.log(err);
        }
    }.bind(this));
};

Robot.prototype.send = function (command) {
    if (this.sendReady) {
        serialPort.write(command, function (err, results) {
            if (err) {
                console.log(err);
            }
            console.log('results ' + results);
        });
    }
};

Robot.prototype.addWheels = function (wheelSettings) {
    var i, settings, wheel;

    for (i = 0; i < wheelSettings.length; i++) {
        settings = wheelSettings[i];
        wheel = new Wheel(settings);
        wheel.robot = this;
        this.wheels.push(wheel);
    }
};

Robot.prototype.findSerialPort = function (callback) {
    var portName = '/dev/ttyACM0';

    /*serialPort.list(function (err, ports) {
        ports.forEach(function(port) {
            console.log(port.comName);
            console.log(port.pnpId);
            console.log(port.manufacturer);
        });
    });*/

    callback(null, portName);
};

Robot.prototype.drive = function (speed, angle, rotation) {
    this.speed = speed;
    this.angle = angle;
    this.rotation = rotation;

    var i, wheel,
        speeds = [];

    for (i = 0; i < this.wheels.length; i++) {
        wheel = this.wheels[i];
        speeds.push(wheel.getSpeedString());
    }

    //console.log(speeds);
    $('#speeds').text(speeds.join(' '));

    this.send('sd' + speeds.join(';'));
};

function Wheel (settings) {
    this.robot = null;

    this.id = settings.id || 0;

    this.angle = settings.angle || 0;
    this.diameter = settings.diameter || 0;

    this.pidFreq = 62.5;
    this.ticksPerRev = 1200;
    this.offset = 0.117;
    this.metricToRobot = this.ticksPerRev / (0.07 * Math.PI) / this.pidFreq;
}

Wheel.prototype.getSpeed = function () {
    this.speed = Math.round((this.robot.speed * Math.cos(this.angle - this.robot.angle) + this.robot.rotation * this.offset)
            * this.metricToRobot);
    return this.speed;
};

Wheel.prototype.getSpeedString = function () {
    this.speed = Math.round(
            (this.robot.speed * Math.cos(this.angle - this.robot.angle) + this.robot.rotation * this.offset)
            / this.metricToRobot
    );
    return this.id + ':' + this.getSpeed();
};

$(document).ready(function () {
    var gamepad = new Gamepad(),
        pidFreq = 60,
        ticksPerRev = 1200,
        wheelCirc = 0.07 * Math.PI,
        wheelOffset = 0.117,
        metricToRobot = ticksPerRev / wheelCirc / pidFreq,
        wheel1Angle = 135 / 180 * Math.PI,
        wheel2Angle = 45 / 180 * Math.PI,
        wheel3Angle = 225 / 180 * Math.PI,
        wheel4Angle = 315 / 180 * Math.PI,
        xSpeed = 0,
        ySpeed = 0,
        rotation = 0,
        dribblerSpeed = 0,
        dribblerOn = false,
        prevSentDribblerSpeed = 0,
        backButton = false,
        driveTimeout;

    console.log('metricToRobot', metricToRobot);

    $(document).keyup(function(e){
        //console.log(e.which);
        switch (e.which) {
        case 116:
            win.reload();
            break;
        case 117:
            win.reloadIgnoringCache();
            break;
        case 122:
            win.toggleKioskMode();
            updateVideoContainer();
            break;
        case 123:
            if (win.isDevToolsOpen()) {
                win.closeDevTools();
            } else {
                win.showDevTools();
            }
            break;
        }
    });

    window.robot = new Robot();

    /*robot.addWheels([
        {angle: 135 / 180 * Math.PI, diameter: 0.07, id: 0},
        {angle: 45 / 180 * Math.PI, diameter: 0.07, id: 1},
        {angle: 225 / 180 * Math.PI, diameter: 0.07, id: 2},
        {angle: 315 / 180 * Math.PI, diameter: 0.07, id: 3}
    ]);*/

    robot.addWheels([
        {angle: 60 / 180 * Math.PI, diameter: 0.07, id: 3},
        {angle: 300 / 180 * Math.PI, diameter: 0.07, id: 4},
        {angle: 180 / 180 * Math.PI, diameter: 0.07, id: 5}
    ]);

    $(window).blur(function() {
        console.log('window blur');
        //socket.emit('drive', {speed: 0, angle: 0, rotation: 0});
        clearTimeout(driveTimeout);
    });

    function drive() {
        var rotationalSpeed = speedMetricToRobot(rotationRadiansToMetersPerSecond(rotation)),
            speed = Math.sqrt(xSpeed * xSpeed + ySpeed * ySpeed),
            angle = Math.atan2(xSpeed, ySpeed),
            wheel1Speed = speedMetricToRobot(wheelSpeed(speed, angle, wheel1Angle)) + rotationalSpeed,
            wheel2Speed = speedMetricToRobot(wheelSpeed(speed, angle, wheel2Angle)) + rotationalSpeed,
            wheel3Speed = speedMetricToRobot(wheelSpeed(speed, angle, wheel3Angle)) + rotationalSpeed,
            wheel4Speed = speedMetricToRobot(wheelSpeed(speed, angle, wheel4Angle)) + rotationalSpeed;
        $('#speed').html('Speed: ' + speed.toFixed(2) + ' m/s');
        $('#angle').html('Angle: ' + (angle * 180 / Math.PI).toFixed(1));
        $('#rotation').html('Rotation: ' + rotation.toFixed(2) + ' rad/s');
        $('#wheel1').html('Wheel1: ' + Math.round(wheel1Speed));
        $('#wheel2').html('Wheel2: ' + Math.round(wheel2Speed));
        $('#wheel3').html('Wheel3: ' + Math.round(wheel3Speed));
        $('#wheel4').html('Wheel4: ' + Math.round(wheel4Speed));

        robot.drive(speed, angle, rotation);

        clearTimeout(driveTimeout);
        driveTimeout = setTimeout(function () {
            drive();
        }, 100);
    }

    function dribbler() {
        var speedDribbler = dribblerOn ? dribblerSpeed : 0;
        if (prevSentDribblerSpeed !== speedDribbler) {
            prevSentDribblerSpeed = speedDribbler;
            //socket.emit('dribbler', {speed: -speedDribbler});
        }
        $('#dribbler').html('Dribbler: ' + speedDribbler);
    }

    function resetUsb() {
        console.log('reset usb');
        //socket.emit('reset usb');
    }

    function wheelSpeed(robotSpeed, robotAngle, wheelAngle) {
        return robotSpeed * Math.cos(wheelAngle - robotAngle);
    }

    function speedMetricToRobot(metersPerSecond) {
        return metersPerSecond * metricToRobot;
    }

    function speedRobotToMetric(wheelSpeed) {
        return wheelSpeed / metricToRobot;
    }

    function rotationRadiansToMetersPerSecond(radiansPerSecond) {
        return radiansPerSecond * wheelOffset;
    }

    gamepad.bind(Gamepad.Event.CONNECTED, function(device) {
        console.log('Connected', device.id);
    });

    gamepad.bind(Gamepad.Event.DISCONNECTED, function(device) {
        console.log('Disconnected', device.id);
    });

    gamepad.bind(Gamepad.Event.UNSUPPORTED, function(device) {
        console.log('Unsupported controller connected', device);
    });

    gamepad.bind(Gamepad.Event.TICK, function(gamepads) {

    });

    gamepad.bind(Gamepad.Event.BUTTON_DOWN, function(e) {
        console.log(e);
        switch(e.mapping) {
            case 4:
                dribblerOn = !dribblerOn;
                dribbler();
                break;
            case 5:
                kick();
                $('#kicker').html('KICK');
                break;
            case 8:
                backButton = true;
                break;
            case 9:
                if (backButton === true) {
                    resetUsb();
                }
                break;
			case 13:
                maxSpeed = 2;
				maxRotation = 4;
				console.log('maxSpeed', maxSpeed);
				console.log('maxRotation', maxRotation);
                break;
			case 14:
                maxSpeed /= 2;
				maxRotation /= 2;
				console.log('maxSpeed', maxSpeed);
				console.log('maxRotation', maxRotation);
                break;
			case 15:
                maxSpeed *= 2;
				maxRotation *= 2;
				console.log('maxSpeed', maxSpeed);
				console.log('maxRotation', maxRotation);
                break;
        }
    });

    gamepad.bind(Gamepad.Event.BUTTON_UP, function(e) {
        console.log(e);
        switch(e.mapping) {
            case 5:
                $('#kicker').html('');
                break;
            case 8:
                backButton = false;
                break;
        }
    });

    gamepad.bind(Gamepad.Event.AXIS_CHANGED, function(e) {
        //console.log(e);
        var prevDribblerSpeed = dribblerSpeed;
        switch(e.mapping) {
            case 0:
                $('#rotate').html('Rotate: ' + e.value);
                rotation = e.value * robot.maxRotation;
                break;
            case 2:
                $('#side').html('X: ' + e.value);
                xSpeed = e.value * robot.maxSpeed;
                break;
            case 3:
                $('#forward').html('Y: ' + (-e.value));
                ySpeed = e.value * robot.maxSpeed;
                break;
            case 6:
                dribblerSpeed = 255 * (0.95 - e.value);
                if (dribblerSpeed < 0) dribblerSpeed = 0;
                if (prevDribblerSpeed < dribblerSpeed) {
                    dribblerSpeed = prevDribblerSpeed;
                }
                dribbler();
                break;
            case 7:
                dribblerSpeed = 255 * e.value;
                if (prevDribblerSpeed > dribblerSpeed) {
                    dribblerSpeed = prevDribblerSpeed;
                }
                dribbler();
                break;
        }
        if (e.mapping === 0 || e.mapping === 2 || e.mapping === 3) {
            drive();
        }
    });

    if (!gamepad.init()) {
        alert('Your browser does not support gamepads, get the latest Google Chrome or Firefox.');
    }
});
