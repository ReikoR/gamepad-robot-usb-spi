var config = {
    developerMode: true,
    videoDir: 'videos',
    converterExecutable: '../../../ffmpeg/ffmpeg.exe',
    audioInput: 'pulse',
    maxVideoLength: 20, //seconds
    maxVideoCount: 200
};

module.exports = config;